package com.davrbk.tourism;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;

/**
 * Created by david on 23.12.16.
 */

public class Tourism extends Application {

    private static Tourism instance;

    private static SharedPreferences sP;

    private RequestQueue mRequestQueue;

    private static Context mContext;

    public static Tourism getInstance() {
        if (instance == null) {
            instance = new Tourism();
        }
        return instance;
    }

    @Override
    public void onCreate() {
        super.onCreate();
        mContext = getApplicationContext();
        sP = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        mRequestQueue = getRequestQueue();
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
    }

    public SharedPreferences getAppsSharedPreferences() {
        return sP;
    }

    public Context getAppsContext() {
        return instance.getApplicationContext();
    }

    public RequestQueue getRequestQueue() {
        if (mRequestQueue == null) {
            // getApplicationContext() is key, it keeps you from leaking the
            // Activity or BroadcastReceiver if someone passes one in.
            mRequestQueue = Volley.newRequestQueue(mContext);
        }
        return mRequestQueue;
    }

    public <T> void addToRequestQueue(Request<T> req) {
        getRequestQueue().add(req);
    }
}
