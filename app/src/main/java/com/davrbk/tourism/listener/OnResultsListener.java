package com.davrbk.tourism.listener;

import citysdk.tourism.client.poi.single.POI;

/**
 * Created by david on 19.12.16.
 */

public interface OnResultsListener {
    void onResultsFinished(POI poi, int id, String parameterTerm, String bytesOfMessage);
}
