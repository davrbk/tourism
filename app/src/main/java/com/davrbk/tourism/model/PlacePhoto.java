package com.davrbk.tourism.model;

/**
 * Created by david on 27.12.16.
 */

public class PlacePhoto {

    public String getHeight() {
        return mHeight;
    }

    public void setHeight(String mHeight) {
        this.mHeight = mHeight;
    }

    public String getWidth() {
        return mWidth;
    }

    public void setWidth(String mWidth) {
        this.mWidth = mWidth;
    }

    public String getRef() {
        return mRef;
    }

    public void setRef(String mRef) {
        this.mRef = mRef;
    }

    public String getRequestUrl() {
        return mUrl;
    }

    public void setRequestUrl(String mUrl) {
        this.mUrl = mUrl;
    }

    private String mHeight;

    private String mWidth;

    private String mRef;

    private String mUrl;

}
