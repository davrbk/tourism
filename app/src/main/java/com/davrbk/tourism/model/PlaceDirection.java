package com.davrbk.tourism.model;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

/**
 * Created by david on 10.01.17.
 */

public class PlaceDirection {

    public String getDistance() {
        return mDistance;
    }

    public void setDistance(String mDistance) {
        this.mDistance = mDistance;
    }

    public List<LatLng> getDirections() {
        return mDirections;
    }

    public void setDirections(List<LatLng> mDirections) {
        this.mDirections = mDirections;
    }

    private String mDistance;

    private List<LatLng> mDirections;
}
