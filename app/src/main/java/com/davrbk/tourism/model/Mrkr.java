package com.davrbk.tourism.model;

import com.google.android.gms.maps.model.LatLng;
import com.google.maps.android.clustering.ClusterItem;

/**
 * Created by david on 27.12.16.
 */

public class Mrkr implements ClusterItem {


    private final LatLng mPosition;

    private String name;

    private String category;

    private String id;

    public Mrkr(double lat, double lng, String name, String category, String id, String base) {
        mPosition = new LatLng(lat, lng);
        this.name = name;
        this.category = category;
        this.id = id;
    }

    @Override
    public LatLng getPosition() {
        return null;
    }

    public String getName() {
        return name;
    }

    public String getCategory() {
        return category;
    }

    public String getId() {
        return id;
    }
}
