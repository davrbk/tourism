package com.davrbk.tourism.model;

import com.google.android.gms.maps.model.LatLng;

import java.util.List;

/**
 * Created by david on 27.12.16.
 */

public class Place {

    public LatLng getLocation() {
        return mLocation;
    }

    public void setLocation(LatLng mLocation) {
        this.mLocation = mLocation;
    }

    public String getId() {
        return mId;
    }

    public void setId(String mId) {
        this.mId = mId;
    }

    public String getPlaceId() {
        return mPlaceId;
    }

    public void setPlaceId(String mPlaceId) {
        this.mPlaceId = mPlaceId;
    }

    public String getName() {
        return mName;
    }

    public void setName(String mName) {
        this.mName = mName;
    }

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String mAddress) {
        this.mAddress = mAddress;
    }

    public String getWebsite() {
        return mWebsite;
    }

    public void setWebsite(String mWebsite) {
        this.mWebsite = mWebsite;
    }

    public String getDistanceTo() {
        return mDistanceTo;
    }

    public void setDistanceTo(String mDistanceTo) {
        this.mDistanceTo = mDistanceTo;
    }

    public int getOpenDays() {
        return mOpenDays;
    }

    public void setOpenDays(int mOpenDays) {
        this.mOpenDays = mOpenDays;
    }

    public String getOpenHours() {
        return mOpenHours;
    }

    public void setOpenHours(String mOpenHours) {
        this.mOpenHours = mOpenHours;
    }

    public float getRating() {
        return mRating;
    }

    public void setRating(float mRating) {
        this.mRating = mRating;
    }

    public String getRef() {
        return mRef;
    }

    public void setRef(String mRef) {
        this.mRef = mRef;
    }

    public String[] getTypes() {
        return mTypes;
    }

    public void setTypes(String[] mTypes) {
        this.mTypes = mTypes;
    }

    public List<PlacePhoto> getPhotos() {
        return mPhotos;
    }

    public void setPhotos(List<PlacePhoto> mPhotos) {
        this.mPhotos = mPhotos;
    }

    private LatLng mLocation;

    private String mId;

    private String mPlaceId;

    private String mName;

    private String mAddress;

    private String mWebsite;

    private String mDistanceTo;

    private int mOpenDays = -1;

    private String mOpenHours;

    private float mRating;

    private String mRef;

    private String[] mTypes;

    private List<PlacePhoto> mPhotos;
}
