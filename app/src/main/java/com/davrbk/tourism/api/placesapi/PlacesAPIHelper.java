package com.davrbk.tourism.api.placesapi;

import com.davrbk.tourism.model.Place;
import com.davrbk.tourism.model.PlaceDirection;
import com.davrbk.tourism.model.PlacePhoto;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

/**
 * Created by david on 09.01.17.
 */

class PlacesAPIHelper {

    static List<Place> parsePlacesFromJson(JSONObject result) throws JSONException {

        List<Place> places = new ArrayList<>();

        JSONArray results = result.getJSONArray("results");

        for (int i = 0; i < results.length(); i++) {
            Place p = new Place();
            JSONObject jO = results.getJSONObject(i);
            p.setId(jO.getString("place_id"));
            try {
                p.setRating((float) jO.getDouble("rating"));
            } catch (JSONException ex) {
                ex.printStackTrace();
            }
            JSONObject loc = jO.getJSONObject("geometry").getJSONObject("location");
            p.setLocation(new LatLng(Double.parseDouble(loc.getString("lat")), Double.parseDouble(loc.getString("lng"))));
            try {
                p.setPhotos(getPlacePhotos(jO));
            } catch (JSONException ex) {
            }
            p.setName(jO.getString("name"));
            p.setRef(jO.getString("reference"));
            p.setAddress(jO.getString("vicinity"));
            p.setTypes(getPlaceTypes(jO));
            places.add(p);
        }
        return places;
    }

    static List<Place> parseTopRatedPlacesFromJson(JSONObject result) throws JSONException {

        List<Place> places = new ArrayList<>();

        JSONArray results = result.getJSONArray("results");

        double rating = -1;

        for (int i = 0; i < results.length(); i++) {
            Place p = new Place();
            JSONObject jO = results.getJSONObject(i);

            try {
                rating = jO.getDouble("rating");
            } catch (JSONException ex) {
                rating = -1;
            }

            if (rating >= 4) { //Getting places with rating from 4
                p.setId(jO.getString("place_id"));
                p.setRating((float) jO.getDouble("rating"));
                JSONObject loc = jO.getJSONObject("geometry").getJSONObject("location");
                p.setLocation(new LatLng(Double.parseDouble(loc.getString("lat")), Double.parseDouble(loc.getString("lng"))));
                try {
                    p.setPhotos(getPlacePhotos(jO));
                } catch (JSONException ex) {
                }
                p.setName(jO.getString("name"));
                p.setRef(jO.getString("reference"));
                p.setAddress(jO.getString("vicinity"));
                p.setTypes(getPlaceTypes(jO));
                places.add(p);
            }
        }
        return places;
    }

    static Place parsePlaceFromJson(JSONObject result) throws JSONException {
        JSONObject placeJson = result.getJSONObject("result");
        Place p = new Place();
        p.setId(placeJson.getString("place_id"));
        try {
            p.setRating((float) placeJson.getDouble("rating"));
        } catch (JSONException ex) {
            ex.printStackTrace();
        }
        JSONObject loc = placeJson.getJSONObject("geometry").getJSONObject("location");
        p.setLocation(new LatLng(Double.parseDouble(loc.getString("lat")), Double.parseDouble(loc.getString("lng"))));
        try {
            p.setPhotos(getPlacePhotos(placeJson));
        } catch (JSONException ex) {
        }
        p.setName(placeJson.getString("name"));
        p.setRef(placeJson.getString("reference"));
        p.setAddress(placeJson.getString("vicinity"));
        p.setTypes(getPlaceTypes(placeJson));
        try {
            p.setWebsite(placeJson.getString("website"));
        } catch (JSONException ex) {
        }

        try {
            p.setOpenDays(getPlaceOpenDays(placeJson));
        } catch (JSONException ex) {
        }
        try {
            p.setOpenHours(getPlaceOpenHours(placeJson));
        } catch (JSONException ex) {

        }

        return p;
    }

    static PlaceDirection placeDistanceFromDirectionJson(JSONObject jO) throws JSONException {

        PlaceDirection pD = new PlaceDirection();

        pD.setDistance(jO.getJSONArray("routes").getJSONObject(0).getJSONArray("legs")
                .getJSONObject(0).getJSONObject("distance").getString("text"));

        JSONArray jArr = jO.getJSONArray("routes").getJSONObject(0).getJSONArray("legs")
                .getJSONObject(0).getJSONArray("steps");

        List<LatLng> directions = new ArrayList<>();

        for (int i = 0; i < jArr.length(); i++) {
            JSONObject item = jArr.getJSONObject(i);
            JSONObject endLoc = item.getJSONObject("end_location");
            JSONObject startLoc = item.getJSONObject("start_location");
            directions.add(new LatLng(startLoc.getDouble("lat"), startLoc.getDouble("lng")));
            directions.add(new LatLng(endLoc.getDouble("lat"), endLoc.getDouble("lng")));
        }

        pD.setDirections(directions);

        return pD;
    }

    private static List<PlacePhoto> getPlacePhotos(JSONObject jO) throws JSONException {

        List<PlacePhoto> ppList = new ArrayList<>();

        JSONArray arr = jO.getJSONArray("photos");

        for (int i = 0; i < arr.length(); i++) {
            PlacePhoto pp = new PlacePhoto();
            JSONObject jPhoto = arr.getJSONObject(i);
            pp.setHeight(jPhoto.getString("height"));
            pp.setWidth(jPhoto.getString("width"));
            String ref = jPhoto.getString("photo_reference");
            pp.setRef(ref);
            pp.setRequestUrl(PlacesApiUrl.buildGetPictureUrl("400", ref));
            ppList.add(pp);
        }
        return ppList;
    }

    private static String[] getPlaceTypes(JSONObject jO) throws JSONException {

        JSONArray arr = jO.getJSONArray("types");

        String[] types = new String[jO.length()];

        for (int i = 0; i < arr.length(); i++) {
            types[i] = arr.getString(i);
        }
        return types;
    }

    private static int getPlaceOpenDays(JSONObject jO) throws JSONException {

        int days = jO.getJSONObject("opening_hours").getJSONArray("periods").length();
//
//        JSONArray jArr = jO.getJSONObject("opening_hours").getJSONArray("periods");
//
//        for (int i = 0; i < jArr.length(); i++) {
//            if (jArr.getJSONObject(i).getJSONObject("open").getInt("day") < 1) {
//                days = jArr.getJSONObject(i).getJSONObject("open").getInt("day") + 1;
//            } else {
//                days = jArr.getJSONObject(i).getJSONObject("open").getInt("day");
//            }
//        }

        return days;
    }

    private static String getPlaceOpenHours(JSONObject jO) throws JSONException {

        String openHours = "";

        String day = "";

        JSONArray jArr = jO.getJSONObject("opening_hours").getJSONArray("weekday_text");

        for (int i = 0; i < jArr.length(); i++) {

            String iHours = "";

            String[] hoursMas = jArr.getString(i).split(" ");
            for (int j = 1; j < hoursMas.length; j++) {
                iHours += hoursMas[j] + " ";
            }

            if (i == 0) {
                openHours = iHours;
            } else if (!openHours.equals(iHours)) {

                String iDay = getShortDayOfWeek(i + 1);

                if (!day.equals(iDay)) {
                    openHours = openHours + "\n" + iDay + ": " + iHours;
                    day = iDay;
                }
            }
        }

        return openHours;
    }

    private static String getShortDayOfWeek(int dayOfWeek) {
        Calendar c = Calendar.getInstance();
        c.set(Calendar.DAY_OF_WEEK, dayOfWeek);
        return c.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.SHORT, Locale.getDefault());
    }
}
