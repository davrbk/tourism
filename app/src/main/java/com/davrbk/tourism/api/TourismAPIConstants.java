package com.davrbk.tourism.api;

/**
 * Created by david on 20.12.16.
 */

public class TourismAPIConstants {

    public static class ResourceURL {
        public static final String LISBOA_PORTUGAL = "http://tourism.citysdk.cm-lisboa.pt/resources";
        public static final String AMSTERDAM_NETHERLANDS = "http://citysdk.dmci.hva.nl/CitySDK/resources";
        public static final String ROME_ITALY = "http://citysdk.inroma.roma.it/CitySDK/resources";
        public static final String LAMIA_GREECE = "http://tourism.citysdk.lamia-city.gr/resources";
    }

    public static class Locale {
        public static final String PT = "pt-PT";
        public static final String NL = "nl-NL";
        public static final String IT = "it-IT";
        public static final String GR = "et-GR";
    }
}
