package com.davrbk.tourism.api.placesapi;

import android.location.Location;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.davrbk.tourism.Tourism;
import com.davrbk.tourism.model.Place;
import com.google.android.gms.maps.model.LatLng;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Iterator;

/**
 * Created by david on 27.12.16.
 */

public class PlacesAPI {

    public interface PlacesCallback {
        void onResponse(Object obj);
    }

    private static PlacesAPI instance;

    public static final String CATEGORY_POI = "point_of_interest";

    public static final String CATEGORY_FOOD = "food,cafe,restaurant,bakery";

    public static final String CATEGORY_DRINK = "bar,cafe";

    public static final String CATEGORY_HOTEL = "lodging";

    public static final String CATEGORY_ART = "art_gallery,movie_theater,painter,zoo";

    public static final String CATEGORY_MUSEUM = "museum";

    public static final String CATEGORY_PARK = "park,zoo,campground";

    private static final String STATUS_ZERO = "ZERO_RESULTS";

    public static final int FLAG_TOP_RATED = 1;

    public static PlacesAPI getInstance() {
        if (instance == null)
            instance = new PlacesAPI();

        return instance;
    }

    public void getPlacesNearby(final String myLocation, final String radius, final String types, final int flags, final PlacesCallback callback) {
        String url = PlacesApiUrl.buildNearbyPlacesUrl(myLocation, radius, types);
        final JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.GET, url, null,
                        new Response.Listener<JSONObject>() {
                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    if (response.getString("status").equals(STATUS_ZERO)) {
                                        callback.onResponse(new ArrayList<Place>());
                                    } else if (flags == FLAG_TOP_RATED) {
                                        callback.onResponse(PlacesAPIHelper.parseTopRatedPlacesFromJson(response));
                                    } else {
                                        callback.onResponse(PlacesAPIHelper.parsePlacesFromJson(response));
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO Auto-generated method stub

                    }
                });
        jsObjRequest.setRetryPolicy(new DefaultRetryPolicy(DefaultRetryPolicy.DEFAULT_TIMEOUT_MS,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES, DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        Tourism.getInstance().addToRequestQueue(jsObjRequest);
    }

    public void getPlaceById(String placeId, final PlacesCallback callback) {
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.GET, PlacesApiUrl.buildGetPlaceInfoByIdUrl(placeId), null,
                        new Response.Listener<JSONObject>() {

                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    callback.onResponse(PlacesAPIHelper.parsePlaceFromJson(response));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO Auto-generated method stub

                    }
                });
        Tourism.getInstance().addToRequestQueue(jsObjRequest);
    }

    public void getDistanceToPoi(LatLng myPoint, LatLng poiPoint, final PlacesCallback callback) {
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.GET, PlacesApiUrl.buildGetDirectionsUrl(myPoint, poiPoint), null,
                        new Response.Listener<JSONObject>() {

                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    callback.onResponse(PlacesAPIHelper.placeDistanceFromDirectionJson(response));
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO Auto-generated method stub

                    }
                });
        Tourism.getInstance().addToRequestQueue(jsObjRequest);
    }

    public void getPlaceDescriptionFromWiki(final LatLng placeLatLng, final PlacesCallback callback) {
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.GET, PlacesApiUrl.WikipediaApi.buildGetWikiSearchByCoordUrl(placeLatLng), null,
                        new Response.Listener<JSONObject>() {

                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    JSONObject jsonObject = response.getJSONObject("query")
                                            .getJSONObject("pages");

                                    Iterator<String> keys = jsonObject.keys();

                                    while (keys.hasNext()) {

                                        String key = keys.next();

                                        Location loc1 = new Location("");
                                        loc1.setLatitude(placeLatLng.latitude);
                                        loc1.setLongitude(placeLatLng.longitude);

                                        Location loc2 = new Location("");
                                        loc2.setLatitude(jsonObject
                                                .getJSONObject(key)
                                                .getJSONArray("coordinates").getJSONObject(0).getDouble("lat"));
                                        loc2.setLongitude(jsonObject
                                                .getJSONObject(key)
                                                .getJSONArray("coordinates").getJSONObject(0).getDouble("lon"));

                                        float distanceInMeters = loc1.distanceTo(loc2);

                                        if (distanceInMeters <= 50) {
                                            String wikiTitle = jsonObject.getJSONObject(key)
                                                    .getString("title").replace(" ", "_");
                                            getPlaceByTitleReq(wikiTitle, callback);
                                        }
                                    }
                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                            }
                        }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO Auto-generated method stub

                    }
                });
        Tourism.getInstance().addToRequestQueue(jsObjRequest);
    }

    private void getPlaceByTitleReq(String title, final PlacesCallback callback) {
        JsonObjectRequest jsObjRequest = new JsonObjectRequest
                (Request.Method.GET, PlacesApiUrl.WikipediaApi.buildGetWikiExtractByTitleUrl(title), null,
                        new Response.Listener<JSONObject>() {

                            @Override
                            public void onResponse(JSONObject response) {
                                try {
                                    JSONObject jsonObject = response.getJSONObject("query")
                                            .getJSONObject("pages");

                                    Iterator<String> keys = jsonObject.keys();
                                    String key = keys.next();

                                    callback.onResponse(jsonObject.getJSONObject(key).getString("extract"));
                                } catch (JSONException ex) {
                                    callback.onResponse("");
                                }
                            }
                        }, new Response.ErrorListener() {

                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // TODO Auto-generated method stub

                    }
                });
        Tourism.getInstance().addToRequestQueue(jsObjRequest);
    }
}
