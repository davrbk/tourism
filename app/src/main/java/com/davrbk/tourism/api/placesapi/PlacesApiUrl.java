package com.davrbk.tourism.api.placesapi;

import com.davrbk.tourism.Constants;
import com.google.android.gms.maps.model.LatLng;

import java.util.Locale;

/**
 * Created by david on 30.12.16.
 */

class PlacesApiUrl {

    private static final String NEARBY_PLACE_SEARCH = "https://maps.googleapis.com/maps/api/place/nearbysearch/json?";

    private static final String GET_PLACE_INFO_BY_ID = "https://maps.googleapis.com/maps/api/place/details/json?placeid=";

    private static final String GET_DIRECTIONS = "https://maps.googleapis.com/maps/api/directions/json?";

    private static final String PARAM_GET_DIR_ORIGIN = "&origin=";

    private static final String PARAM_GET_DIR_DEST = "&destination=";

    private static final String PARAM_GET_DIR_UNITS_METRICS = "&units=metrics";

    private static final String PARAM_GET_DIR_MODE_WALKING = "&mode=walking";

    private static final String GET_PLACE_PIC = "https://maps.googleapis.com/maps/api/place/photo?";

    private static final String PARAM_LOCATION = "location=";

    private static final String PARAM_RADIUS = "&radius=";

    private static final String PARAM_TYPES = "&types=";

    private static final String PARAM_NAME = "&name=";

    private static final String KEY = "&key=" + Constants.GoogleApi.PLACES_API_KEY;

    private static final String SENSOR = "&sensor=true";

    private static final String PARAM_MAX_WIDTH = "maxwidth=";

    private static final String PARAM_PHOTO_REF = "&photoreference=";


    static String buildNearbyPlacesUrl(String myLocation, String radius, String types) {
        return NEARBY_PLACE_SEARCH + PARAM_LOCATION + myLocation + PARAM_RADIUS + radius + PARAM_TYPES + types + KEY;
    }

    static String buildGetPictureUrl(String maxWidth, String photoRef) {
        return GET_PLACE_PIC + PARAM_MAX_WIDTH + maxWidth + PARAM_PHOTO_REF + photoRef + KEY;
    }

    static String buildGetPlaceInfoByIdUrl(String placeId) {
        return GET_PLACE_INFO_BY_ID + placeId + KEY;
    }

    static String buildGetDirectionsUrl(LatLng myPoint, LatLng dest) {
        return GET_DIRECTIONS + PARAM_GET_DIR_ORIGIN + myPoint.latitude + "," + myPoint.longitude
                + PARAM_GET_DIR_DEST + dest.latitude + "," + dest.longitude + PARAM_GET_DIR_UNITS_METRICS
                + PARAM_GET_DIR_MODE_WALKING + KEY;
    }

    static class WikipediaApi {

        private static final String WIKI_FIND_PLACE_BY_COORD = "https://" + Locale.getDefault().getLanguage()
                + ".wikipedia.org/w/api.php?format=json&action=query&prop=coordinates&generator=geosearch&ggscoord=";

        private static final String WIKI_PARAM_RADIUS_500 = "&ggsradius=200";

        private static final String WIKI_PARAM_LIMIT = "&ggslimit=2";

        private static final String WIKI_GET_EXTRACT_BY_TITLE = "https://" + Locale.getDefault().getLanguage()
                + ".wikipedia.org/w/api.php?format=json&action=query&prop=extracts&exintro=&explaintext=&titles=";

        static String buildGetWikiSearchByCoordUrl(LatLng placeLatLng) {

            return WIKI_FIND_PLACE_BY_COORD  + placeLatLng.latitude + "|" + placeLatLng.longitude
                    + WIKI_PARAM_RADIUS_500 + WIKI_PARAM_LIMIT;
        }

        static String buildGetWikiExtractByTitleUrl(String title) {
            return WIKI_GET_EXTRACT_BY_TITLE + title;
        }
    }
}
