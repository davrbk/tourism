package com.davrbk.tourism.view;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.util.AttributeSet;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.davrbk.tourism.R;

/**
 * Created by david on 18.12.16.
 */

public class TitleBarView extends LinearLayout {

    private TextView mTitleView;
    private View mStopPlaying, mBurger, mMore;
    private DrawerLayout mDrawer;
//    private CircularProgressView mPlayingProgress;
//    private TimeHandler mTimeHandler;

    public TitleBarView(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    @Override
    protected void onFinishInflate() {
        super.onFinishInflate();

//        mTimeHandler = new TimeHandler();

        mTitleView = (TextView) findViewById(R.id.title);
        mBurger = findViewById(R.id.burger);
        mMore = findViewById(R.id.more);
//        mBack = findViewById(R.id.back);
//        mStopPlaying = findViewById(R.id.stop);
//        mPlayingProgress = (CircularProgressView) findViewById(R.id.playing_progress);

        mBurger.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mDrawer == null) {
                    return;
                }
                if (mDrawer.isDrawerOpen(GravityCompat.START)) {
                    mDrawer.closeDrawer(GravityCompat.START);
                } else {
                    mDrawer.openDrawer(GravityCompat.START);
                }
            }
        });

//        mBack.setOnClickListener(new OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                ((Activity) getContext()).onBackPressed();
//            }
//        });
    }

    public void setOnStopClickListener(View.OnClickListener listener) {
        setPlaying(false);
        mStopPlaying.setOnClickListener(listener);
    }

    public void setOnMoreClicked(View.OnClickListener listener) {
        if (mMore != null)
            mMore.setOnClickListener(listener);
    }

    public void toggleMoreVisibility(boolean visible) {
        if (mMore != null) {
            if (visible) {
                mMore.setVisibility(VISIBLE);
            } else {
                mMore.setVisibility(GONE);
            }
        }
    }

    public void setPlaying(boolean isPlaying) {
        mStopPlaying.setVisibility(isPlaying ? View.VISIBLE : INVISIBLE);
//        if (mPlayingProgress.getMaxProgress() == 0) {
//            return;
//        }
//        if (isPlaying) {
//            mTimeHandler.sendEmptyMessageDelayed(1, 1000);
//        } else {
//            mTimeHandler.sendEmptyMessage(0);
//        }
    }

    public void setDrawerLayout(DrawerLayout mDrawer) {
        this.mDrawer = mDrawer;
    }

    public void setTitle(int title) {
        setTitle(getContext().getResources().getString(title));
    }

    public void setTitle(String title) {
        mTitleView.setText(title);
    }

    public void setBackFragment(boolean isBack) {
        mBurger.setVisibility(isBack ? GONE : VISIBLE);
//        mBack.setVisibility(isBack ? VISIBLE : GONE);
    }

//    public void initProgress(int max) {
//        mPlayingProgress.setProgress(0);
//        mPlayingProgress.setMaxProgress(max);
//    }

//    public void increaseProgress() {
//        if (mPlayingProgress.getProgress() < mPlayingProgress.getMaxProgress()) {
//            mPlayingProgress.setProgress(mPlayingProgress.getProgress() + 1);
//        }
//    }

//    private class TimeHandler extends Handler {
//
//        @Override
//        public void handleMessage(Message msg) {
//            if (msg.what == 1) {
//                increaseProgress();
//                sendEmptyMessageDelayed(1, 1000);
//            }
//        }
//    }
}