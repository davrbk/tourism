package com.davrbk.tourism.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.davrbk.tourism.R;
import com.davrbk.tourism.activity.ShowMoreInfoActivity;
import com.davrbk.tourism.helper.DisplayHelper;
import com.davrbk.tourism.model.Place;
import com.squareup.picasso.Picasso;

/**
 * Created by david on 16.01.17.
 */

public class NearbyListItemView extends FrameLayout {

    public NearbyListItemView(final Activity activity, final Place p) {
        super(activity);

        setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, DisplayHelper.dpToPx(activity, 105)));

        ImageView iv = new ImageView(activity);
        iv.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        iv.setImageResource(R.drawable.bkg_poi);
        iv.setScaleType(ImageView.ScaleType.CENTER_CROP);

        if (p.getPhotos() != null)
            Picasso.with(activity)
                    .load(p.getPhotos().get(0).getRequestUrl())
                    .into(iv);

        addView(iv);

        View v = new View(activity);
        v.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.MATCH_PARENT));
        v.setBackgroundColor(activity.getResources().getColor(R.color.itemMask));
        addView(v);

        TextView tvName = new TextView(activity);
        LayoutParams tvLayoutParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        tvLayoutParams.gravity = Gravity.CENTER_VERTICAL;
        tvLayoutParams.leftMargin = DisplayHelper.dpToPx(activity, 13);
        tvName.setLayoutParams(tvLayoutParams);
        tvName.setTextColor(activity.getResources().getColor(android.R.color.white));
        tvName.setTextSize(18);
        tvName.setText(p.getName());
        addView(tvName);

        FrameLayout rl = new FrameLayout(activity);
        ContextThemeWrapper contextThemeWrapper = new ContextThemeWrapper(activity, R.style.RatingBar);
        RatingBar r = new RatingBar(contextThemeWrapper, null, 0);
        LayoutParams rbLayoutParams =
                new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        rbLayoutParams.gravity = Gravity.BOTTOM;
        rbLayoutParams.leftMargin = DisplayHelper.dpToPx(activity, 13);
        rbLayoutParams.bottomMargin = DisplayHelper.dpToPx(activity, 18);
        rl.setLayoutParams(rbLayoutParams);
        r.setIsIndicator(true);
        r.setMax(5);
        r.setNumStars(5);
        r.setStepSize(0.1f);
        r.setRating(p.getRating());
        rl.addView(r);
        addView(rl);

        TextView tvDistance = new TextView(activity);
        LayoutParams tvDLayoutParams = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
        tvDLayoutParams.gravity = Gravity.END;
        tvDLayoutParams.topMargin = DisplayHelper.dpToPx(activity, 12);
        tvDLayoutParams.rightMargin = DisplayHelper.dpToPx(activity, 14);
        tvDistance.setLayoutParams(tvDLayoutParams);
        tvDistance.setTextColor(activity.getResources().getColor(R.color.textAccent));
        tvDistance.setTextSize(14);
        tvDistance.setText(p.getDistanceTo());
        addView(tvDistance);

        this.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent i = new Intent(activity, ShowMoreInfoActivity.class);
                i.putExtra("id", p.getId());
                i.putExtra("name", p.getName());
                activity.finish();
                activity.startActivity(i);
            }
        });
    }
}
