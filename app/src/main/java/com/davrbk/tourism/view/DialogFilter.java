package com.davrbk.tourism.view;

import android.app.Dialog;
import android.content.Context;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Checkable;

import com.davrbk.tourism.R;

/**
 * Created by david on 21.12.16.
 */

public class DialogFilter extends Dialog {

    public final static String ART = "art";

    public final static String HISTORICAL = "historical";

    public final static String ARCHITECTURE = "architecture";

    public final static String FREE = "free";

    public final static String TOP = "top";

    public final static String PARKS = "parks";

    public final static String EAT = "eat";

    public final static String DRINK = "drink";

    public final static String HOTEL = "hotel";

    public final static String ROMANTIC = "romantic";

    public interface OnDialogItemCheckedListener {

        void onArtChecked();

        void onHistoricalChecked();

        void onArchitectureChecked();

        void onFreeChecked();

        void onTopChecked();

        void onParksChecked();

        void onEatChecked();

        void onDrinkChecked();

        void onHotelChecked();

        void onRomanticChecked();
    }

    private OnDialogItemCheckedListener mListener;

    private Checkable mArt;

    public DialogFilter(Context context, int themeResId) {
        super(context, themeResId);

        View v = ((LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE))
                .inflate(R.layout.dialog_filter, null);

        if (getWindow() != null) {
            getWindow().setGravity(Gravity.BOTTOM);
            getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            getWindow().setContentView(v);
        }

        v.findViewById(R.id.dialogFilter_art).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mListener != null)
                    mListener.onArtChecked();
                dismiss();
            }
        });
        v.findViewById(R.id.dialogFilter_historical).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mListener != null)
                    mListener.onHistoricalChecked();
                dismiss();
            }
        });
        v.findViewById(R.id.dialogFilter_architecture).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mListener != null)
                    mListener.onArchitectureChecked();
                dismiss();
            }
        });
        v.findViewById(R.id.dialogFilter_free).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mListener != null)
                    mListener.onFreeChecked();
                dismiss();
            }
        });
        v.findViewById(R.id.dialogFilter_top).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mListener != null)
                    mListener.onTopChecked();
                dismiss();
            }
        });
        v.findViewById(R.id.dialogFilter_parks).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mListener != null)
                    mListener.onParksChecked();
                dismiss();
            }
        });
        v.findViewById(R.id.dialogFilter_eat).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mListener != null)
                    mListener.onEatChecked();
                dismiss();
            }
        });
        v.findViewById(R.id.dialogFilter_drink).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mListener != null)
                    mListener.onDrinkChecked();
                dismiss();
            }
        });
        v.findViewById(R.id.dialogFilter_hotel).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mListener != null)
                    mListener.onHotelChecked();
                dismiss();
            }
        });
        v.findViewById(R.id.dialogFilter_romantic).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (mListener != null)
                    mListener.onRomanticChecked();
                dismiss();
            }
        });
    }

    public void setOnDialogItemCheckedListener(OnDialogItemCheckedListener listener) {
        mListener = listener;
    }

//    @Override
//    public void onClick(View view) {
//
//        switch (view.getId()) {
//            case R.id.dialogFilter_art:
//                if (mListener != null)
//                    mListener.onArtChecked();
//                this.dismiss();
//                break;
//
//            case R.id.dialogFilter_historical:
//                if (mListener != null)
//                    mListener.onHistoricalChecked();
//                this.dismiss();
//                break;
//
//            case R.id.dialogFilter_architecture:
//                if (mListener != null)
//                    mListener.onArchitectureChecked();
//                this.dismiss();
//                break;
//
//            case R.id.dialogFilter_free:
//                if (mListener != null)
//                    mListener.onFreeChecked();
//                this.dismiss();
//                break;
//
//            case R.id.dialogFilter_top:
//                if (mListener != null)
//                    mListener.onTopChecked();
//                this.dismiss();
//                break;
//
//            case R.id.dialogFilter_parks:
//                if (mListener != null)
//                    mListener.onParksChecked();
//                this.dismiss();
//                break;
//
//            case R.id.dialogFilter_eat:
//                if (mListener != null)
//                    mListener.onEatChecked();
//                this.dismiss();
//                break;
//
//            case R.id.dialogFilter_drink:
//                if (mListener != null)
//                    mListener.onDrinkChecked();
//                this.dismiss();
//                break;
//
//            case R.id.dialogFilter_hotel:
//                if (mListener != null)
//                    mListener.onHotelChecked();
//                this.dismiss();
//                break;
//
//            case R.id.dialogFilter_romantic:
//                if (mListener != null)
//                    mListener.onRomanticChecked();
//                this.dismiss();
//                break;
//        }
//    }
}
