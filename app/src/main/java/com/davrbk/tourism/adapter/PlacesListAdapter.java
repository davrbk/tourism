package com.davrbk.tourism.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.RatingBar;
import android.widget.TextView;

import com.davrbk.tourism.R;
import com.davrbk.tourism.model.Place;
import com.squareup.picasso.Picasso;

import java.util.List;

/**
 * Created by david on 13.01.17.
 */

public class PlacesListAdapter extends ArrayAdapter {

    private List<Place> mPlaces;

    private Context mContext;

    private ViewHolder holder;

    public PlacesListAdapter(Context context, int resource, List<Place> objects) {
        super(context, resource, objects);

        mPlaces = objects;
        mContext = context;

    }

    @Override
    public Place getItem(int position) {
        return mPlaces.get(position);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View v = convertView;

        if (v == null) {
            v = ((LayoutInflater) mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE))
                    .inflate(R.layout.list_places_item, parent, false);
            holder          = new ViewHolder();
            holder.iv       = (ImageView) v.findViewById(R.id.placeItemBck);
            holder.title    = (TextView) v.findViewById(R.id.placeItemTitle);
            holder.distance = (TextView) v.findViewById(R.id.placeItemDistance);
            holder.rb       = (RatingBar) v.findViewById(R.id.placeItemRating);

            v.setTag(holder);
        } else {
            holder = (ViewHolder) v.getTag();
        }

        Place p = getItem(position);

        if (p != null) {
            if (holder.iv != null && p.getPhotos() != null) {
                Picasso.with(mContext)
                        .load(p.getPhotos().get(0).getRequestUrl())
                        .into(holder.iv);
            }

            if (holder.title != null) {
                holder.title.setText(p.getName());
            }

            if (holder.distance != null) {
                holder.distance.setText(p.getDistanceTo());
            }

            if (holder.rb != null) {
                holder.rb.setRating(p.getRating());
            }
        }

        return v;
    }

    private class ViewHolder {

        ImageView iv;
        TextView title, distance;
        RatingBar rb;
    }
}
