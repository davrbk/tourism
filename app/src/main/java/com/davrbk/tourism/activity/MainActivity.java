package com.davrbk.tourism.activity;

import android.graphics.Color;
import android.support.annotation.AnimRes;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.davrbk.tourism.R;
import com.davrbk.tourism.fragment.AllItinerariesFragment;
import com.davrbk.tourism.fragment.InfoFragment;
import com.davrbk.tourism.fragment.MainMapFragment;
import com.davrbk.tourism.fragment.ProfileFragment;
import com.davrbk.tourism.fragment.SettingsFragment;
import com.davrbk.tourism.fragment.UsersNearbyFragment;
import com.davrbk.tourism.helper.DisplayHelper;
import com.davrbk.tourism.view.TitleBarView;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    private View mRoot;

    private DrawerLayout mDrawer;

    private static TitleBarView mTitleBarView;

    public DrawerLayout getDrawer() {
        return mDrawer;
    }

    public static TitleBarView getTitleBarView() {
        return mTitleBarView;
    }

    private NavigationView mNavigationView;

    private static MainActivity sInstance;

    public static MainActivity getInstance() {
        return sInstance;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mRoot = findViewById(R.id.root);
        mTitleBarView = (TitleBarView) mRoot.findViewById(R.id.title_bar);
        mDrawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        mTitleBarView.setDrawerLayout(mDrawer);
        mDrawer.setScrimColor(Color.TRANSPARENT);
        mDrawer.setDrawerElevation(0);
        mDrawer.addDrawerListener(new DrawerLayout.DrawerListener() {
            @Override
            public void onDrawerSlide(View drawerView, float slideOffset) {
                mRoot.setTranslationX(DisplayHelper.dpToPx(MainActivity.this, 280) * slideOffset);
            }

            @Override
            public void onDrawerOpened(View drawerView) {

            }

            @Override
            public void onDrawerClosed(View drawerView) {

            }

            @Override
            public void onDrawerStateChanged(int newState) {

            }
        });

        mNavigationView = (NavigationView) findViewById(R.id.nav_view);
        mNavigationView.setNavigationItemSelectedListener(this);
        Menu m = mNavigationView.getMenu();
        for (int i = 0; i < m.size(); i++) {
//            FontHelper.applyFontToMenuItem(this, m.getItem(i));
        }

        getSupportFragmentManager().beginTransaction().replace(R.id.container,
                new MainMapFragment()).commit();
        setNavigationCheckedItem(0);
    }

    @Override
    protected void onStart() {
        super.onStart();
        sInstance = MainActivity.this;
    }

    @Override
    protected void onStop() {
        sInstance = null;
        super.onStop();
    }

    @Override
    public void onBackPressed() {
        if (mDrawer.isDrawerOpen(GravityCompat.START)) {
            mDrawer.closeDrawer(GravityCompat.START);
            return;
        }
        super.onBackPressed();
    }

    public void setNavigationCheckedItem(int position) {
        mNavigationView.getMenu().getItem(position).setChecked(true);
    }

    public Fragment getCurrentFragment() {
        return getSupportFragmentManager().findFragmentById(R.id.container);
    }

    public void replaceFragment(Fragment fragment) {
        replaceFragment(fragment, 0, 0, 0, 0);
    }

    public void replaceFragment(Fragment fragment, @AnimRes int enter,
                                @AnimRes int exit, @AnimRes int popEnter, @AnimRes int popExit) {
        String backStateName = fragment.getClass().getName();

        FragmentManager manager = getSupportFragmentManager();
        boolean fragmentPopped = manager.popBackStackImmediate(backStateName, 0);

        if (!fragmentPopped) {
            FragmentTransaction ft = manager.beginTransaction();
            ft.setCustomAnimations(enter, exit, popEnter, popExit);
            ft.replace(R.id.container, fragment);
            ft.addToBackStack(backStateName);
            ft.commitAllowingStateLoss();
        }
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.nav_poi && !(getCurrentFragment() != null &&
                getCurrentFragment() instanceof MainMapFragment)) {
            replaceFragment(new MainMapFragment());
        } else if (id == R.id.nav_all_itiner && !(getCurrentFragment() != null &&
                getCurrentFragment() instanceof AllItinerariesFragment)) {
            replaceFragment(new AllItinerariesFragment());
        } else if (id == R.id.nav_users && !(getCurrentFragment() != null &&
                getCurrentFragment() instanceof UsersNearbyFragment)) {
            replaceFragment(new UsersNearbyFragment());
        } else if (id == R.id.nav_info && !(getCurrentFragment() != null &&
                getCurrentFragment() instanceof InfoFragment)) {
            replaceFragment(new InfoFragment());
        } else if (id == R.id.nav_settings && !(getCurrentFragment() != null &&
                getCurrentFragment() instanceof SettingsFragment)) {
            replaceFragment(new SettingsFragment());
        } else if (id == R.id.nav_profile && !(getCurrentFragment() != null &&
                getCurrentFragment() instanceof ProfileFragment)) {
            replaceFragment(new ProfileFragment());
        } else if (id == R.id.nav_download) {
//            AlertHelper.showDialogWithTwoButtons(this, true, -1, R.string.sign_out_, R.string.sign_out,
//                    R.string.cancel, new AlertHelper.OnActionListener() {
//                        @Override
//                        public void onPositive(Object o) {
//                            signOut();
//                        }
//                    });
        }
        mDrawer.closeDrawer(GravityCompat.START);
        return false;
    }
}
