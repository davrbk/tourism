package com.davrbk.tourism.activity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentActivity;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.balysv.materialripple.MaterialRippleLayout;
import com.davrbk.tourism.Constants;
import com.davrbk.tourism.R;
import com.davrbk.tourism.Tourism;
import com.davrbk.tourism.helper.NetworkHelper;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FacebookAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.GoogleAuthProvider;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;

import com.davrbk.tourism.helper.AlertHelper;

import java.util.List;

/**
 * Created by david on 09.12.16.
 */

public class LoginActivity extends FragmentActivity implements Validator.ValidationListener,
        GoogleApiClient.OnConnectionFailedListener, View.OnClickListener, CompoundButton.OnCheckedChangeListener {

    private static final int RC_SIGN_IN = 9001;

    @NotEmpty
    @Email
    private EditText mEmail;

    @NotEmpty
    @Password(min = 6, scheme = Password.Scheme.ANY)
    private EditText mPass;

    private CallbackManager mCallbackManager;

    private Validator validator;

    private boolean mRemember;

    MaterialDialog mProgress;

    FirebaseAuth mAuth;

    FirebaseAuth.AuthStateListener mAuthListener = new FirebaseAuth.AuthStateListener() {
        @Override
        public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {

        }
    };

    private GoogleApiClient mGoogleApiClient;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        if (Tourism.getInstance().getAppsSharedPreferences().getBoolean(Constants.LOGGED_IN, false)) {
            startActivity(new Intent(this, MainActivity.class));
            finish();
        }

        initViews();
        validator = new Validator(this);
        validator.setValidationListener(this);
        mAuth = FirebaseAuth.getInstance();

        GoogleSignInOptions gso = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestIdToken(Constants.GoogleApi.CLIENT_ID)
                .requestEmail()
                .build();

        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .enableAutoManage(this /* FragmentActivity */, this /* OnConnectionFailedListener */)
                .addApi(Auth.GOOGLE_SIGN_IN_API, gso)
                .build();

        mCallbackManager = CallbackManager.Factory.create();
        LoginButton loginButton = (LoginButton) findViewById(R.id.facebookSignIn);
        loginButton.setReadPermissions("email", "public_profile");
        loginButton.setOnClickListener(this);
        loginButton.registerCallback(mCallbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                handleFacebookAccessToken(loginResult.getAccessToken());
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

            }
        });
    }

    public void onSignUpActivityClicked(View v) {
        startActivity(new Intent(this, SignUpActivity.class));
    }

    private void initViews() {
        mEmail = (EditText) findViewById(R.id.loginEmailEdit);
        mPass = (EditText) findViewById(R.id.loginPassEdit);
        CheckBox remember = (CheckBox) findViewById(R.id.rememberCheckBox);
        remember.setOnCheckedChangeListener(this);
        remember.setChecked(false);
        findViewById(R.id.googleSignIn).setOnClickListener(this);
        findViewById(R.id.loginButton).setOnClickListener(this);
    }

    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mAuthListener != null)
            mAuth.removeAuthStateListener(mAuthListener);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        // Result returned from launching the Intent from GoogleSignInApi.getSignInIntent(...);
        if (requestCode == RC_SIGN_IN) {
            GoogleSignInResult result = Auth.GoogleSignInApi.getSignInResultFromIntent(data);
            if (result.isSuccess()) {
                // Google Sign In was successful, authenticate with Firebase
                GoogleSignInAccount account = result.getSignInAccount();
                firebaseAuthWithGoogle(account);
            } else {
                // Google Sign In failed, update UI appropriately
                // ...
            }
        }
        mCallbackManager.onActivityResult(requestCode, resultCode, data);
    }

    private void firebaseAuthWithGoogle(GoogleSignInAccount acct) {

        AuthCredential credential = GoogleAuthProvider.getCredential(acct.getIdToken(), null);
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {

                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (task.isSuccessful()) {
                            mProgress.dismiss();
                            startActivity(new Intent(LoginActivity.this, MainActivity.class));
                            finish();
                            if (mRemember) {
                                Tourism.getInstance()
                                        .getAppsSharedPreferences().edit()
                                        .putBoolean(Constants.LOGGED_IN, mRemember)
                                        .apply();
                            }
//                            Toast.makeText(GoogleSignInActivity.this, "Authentication failed.",
//                                    Toast.LENGTH_SHORT).show();
                        }
                        // ...
                    }
                });
    }

    @Override
    public void onValidationSucceeded() {
        if (NetworkHelper.isOnline(this, NetworkHelper.ALERT)) {
            mProgress = AlertHelper.getProgressDialog(this, getString(R.string.progress_wait));
            mProgress.show();
            mAuth.signInWithEmailAndPassword(mEmail.getText().toString(), mPass.getText().toString())
                    .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                        @Override
                        public void onComplete(@NonNull Task<AuthResult> task) {
                            if (task.isSuccessful()) {
                                mProgress.dismiss();
                                startActivity(new Intent(LoginActivity.this, MainActivity.class));
                                finish();
                                if (mRemember) {
                                    Tourism.getInstance()
                                            .getAppsSharedPreferences().edit()
                                            .putBoolean(Constants.LOGGED_IN, mRemember)
                                            .apply();
                                }
                            } else {
                                mProgress.dismiss();
                                String m = task.getException().getMessage();
                                Toast.makeText(LoginActivity.this, m, Toast.LENGTH_SHORT).show();
                                //TODO: An error occurred
                            }
                        }
                    });
        }
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    private void googleSignIn() {
        if (NetworkHelper.isOnline(this, NetworkHelper.ALERT)) {
            mProgress = AlertHelper.getProgressDialog(this, getString(R.string.progress_wait));
            mProgress.show();
            Intent signInIntent = Auth.GoogleSignInApi.getSignInIntent(mGoogleApiClient);
            startActivityForResult(signInIntent, RC_SIGN_IN);
        }
    }

    private void loginFireBase() {
        validator.validate();
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.googleSignIn:
                googleSignIn();
                break;

            case R.id.loginButton:
                loginFireBase();
                break;

            case R.id.facebookSignIn:
                if (NetworkHelper.isOnline(this, NetworkHelper.ALERT)) {
                    mProgress = AlertHelper.getProgressDialog(this, getString(R.string.progress_wait));
                    mProgress.show();
                }
                break;
        }
    }

    private void handleFacebookAccessToken(AccessToken token) {

        AuthCredential credential = FacebookAuthProvider.getCredential(token.getToken());
        mAuth.signInWithCredential(credential)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        mProgress.dismiss();
                        startActivity(new Intent(LoginActivity.this, MainActivity.class));
                        finish();
                        if (mRemember) {
                            Tourism.getInstance()
                                    .getAppsSharedPreferences().edit()
                                    .putBoolean(Constants.LOGGED_IN, mRemember)
                                    .apply();
                        }
                        // If sign in fails, display a message to the user. If sign in succeeds
                        // the auth state listener will be notified and logic to handle the
                        // signed in user can be handled in the listener.
                        if (!task.isSuccessful()) {
//                            Log.w(TAG, "signInWithCredential", task.getException());
//                            Toast.makeText(FacebookLoginActivity.this, "Authentication failed.",
//                                    Toast.LENGTH_SHORT).show();
                        }

                        // ...
                    }
                });
    }

    @Override
    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
        mRemember = b;
        compoundButton.setChecked(b);
    }
}
