package com.davrbk.tourism.activity;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.davrbk.tourism.R;
import com.davrbk.tourism.helper.RoundedCornersTransform;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.UserProfileChangeRequest;
import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.ConfirmPassword;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Password;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;
import com.squareup.picasso.Picasso;
import com.theartofdev.edmodo.cropper.CropImage;
import com.theartofdev.edmodo.cropper.CropImageView;

import java.util.List;

/**
 * Created by david on 16.12.16.
 */

public class SignUpActivity extends Activity implements Validator.ValidationListener {

    private static String TAG = "SignUpActivity";

    static final int REQUEST_IMAGE_PICK = 1;

    @NotEmpty
    private EditText mFName;

    @NotEmpty
    private EditText mLName;

    @NotEmpty
    @Email
    private EditText mEmail;

    @NotEmpty
    @Password(min = 6, scheme = Password.Scheme.ANY)
    private EditText mPass;

    @NotEmpty
    @ConfirmPassword
    private EditText mConfPass;

    private Validator validator;

    private FirebaseAuth mAuth;

    private FirebaseAuth.AuthStateListener mAuthListener = new FirebaseAuth.AuthStateListener() {
        @Override
        public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth) {
            FirebaseUser user = firebaseAuth.getCurrentUser();
            if (user != null) {
                // User is signed in
                UserProfileChangeRequest profileUpdates = new UserProfileChangeRequest.Builder()
                        .setDisplayName(mFName + " " + mLName)
                        /*.setPhotoUri(Uri.parse("https://example.com/jane-q-user/profile.jpg"))*/
                        .build();
                user.updateProfile(profileUpdates);
                Log.d(TAG, "onAuthStateChanged:signed_in:" + user.getUid());
            } else {
                // User is signed out
                Log.d(TAG, "onAuthStateChanged:signed_out");
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        mAuth = FirebaseAuth.getInstance();

        validator = new Validator(this);
        validator.setValidationListener(this);
        initViews();
    }

    public void onSignUpClicked(View v) {
        validator.validate();
    }

    public void onSignInClicked(View v) {
        onBackPressed();
    }

    public void onPictureClicked(View v) {

        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
        intent.setType("image/*");
        startActivityForResult(intent, REQUEST_IMAGE_PICK);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_IMAGE_PICK && resultCode == Activity.RESULT_OK) {
            cropImage(data.getData());
        } else if (requestCode == CropImage.CROP_IMAGE_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
            String uri = CropImage.getActivityResult(data).getUri().toString();
            loadImageIntoView(uri);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        mAuth.addAuthStateListener(mAuthListener);
    }

    @Override
    protected void onStop() {
        super.onStop();
        if (mAuthListener != null)
            mAuth.removeAuthStateListener(mAuthListener);
    }

    @Override
    public void onValidationSucceeded() {
        //TODO: Sign Up
        mAuth.createUserWithEmailAndPassword(mEmail.getText().toString(), mPass.getText().toString())
                .addOnCompleteListener(new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
//                            Toast.makeText(SignUpActivity.this, "YOU HAD SIGNED UP!", Toast.LENGTH_LONG).show();
                        }
                    }
                });
    }

    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            // Display error messages ;)
            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }

    private void cropImage(Uri imageUri) {
        CropImage.activity(imageUri)
                .setGuidelines(CropImageView.Guidelines.ON)
                .setAspectRatio(1, 1)
                .start(this);
    }

    private void loadImageIntoView(String uri) {
        Picasso.with(this).load(uri)
                .fit()
                .transform(new RoundedCornersTransform())
                .into((ImageView) findViewById(R.id.signUpProfilePic));
    }

    private void initViews() {
        mFName = (EditText) findViewById(R.id.signUpFirstNameEdit);
        mLName = (EditText) findViewById(R.id.signUpLastNameEdit);
        mEmail = (EditText) findViewById(R.id.signUpEmailEdit);
        mPass = (EditText) findViewById(R.id.signUpPassEdit);
        mConfPass = (EditText) findViewById(R.id.signUpConfirmPassEdit);
    }
}
