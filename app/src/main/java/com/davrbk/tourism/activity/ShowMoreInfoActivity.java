package com.davrbk.tourism.activity;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.location.Location;
import android.net.Uri;
import android.os.Bundle;
import android.os.Parcelable;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.CollapsingToolbarLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.RatingBar;
import android.widget.TextView;

import com.davrbk.tourism.R;
import com.davrbk.tourism.api.placesapi.PlacesAPI;
import com.davrbk.tourism.helper.DisplayHelper;
import com.davrbk.tourism.model.Place;
import com.davrbk.tourism.model.PlaceDirection;
import com.davrbk.tourism.model.PlacePhoto;
import com.davrbk.tourism.view.NearbyListItemView;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.assist.ImageScaleType;
import com.nostra13.universalimageloader.core.display.FadeInBitmapDisplayer;
import com.nostra13.universalimageloader.core.listener.SimpleImageLoadingListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by david on 20.12.16.
 */

public class ShowMoreInfoActivity extends AppCompatActivity implements PlacesAPI.PlacesCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        OnMapReadyCallback {

    DisplayImageOptions options;

    ViewPager pager;

    Toolbar toolbar;

    private ArrayList<LatLng> mPoints;

    private SupportMapFragment mapFragment;

    private View mMapView;

    private GoogleMap mMap;

    private GoogleApiClient mLocationClient;

    private Location mLastLocation;

    private PlacesAPI mApi;

    int positionGeral = 1;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.act_show_more_info);

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(
                getApplicationContext()).build();
        ImageLoader.getInstance().init(config);

        Bundle bundle = getIntent().getExtras();
        assert bundle != null;

        options = new DisplayImageOptions.Builder()
                .showImageForEmptyUri(R.drawable.ic_missing_img)
                .showImageOnFail(R.drawable.ic_broken_link)
                .resetViewBeforeLoading(true)
                .cacheOnDisc(true)
                .imageScaleType(ImageScaleType.IN_SAMPLE_POWER_OF_2)
                .bitmapConfig(Bitmap.Config.ARGB_8888)
                .considerExifParams(true)
                .displayer(new FadeInBitmapDisplayer(300))
                .build();

        pager = (ViewPager) findViewById(R.id.pager);

        toolbar = (Toolbar) findViewById(R.id.toolbar);

        setSupportActionBar(toolbar);

        if (getSupportActionBar() != null) {
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeAsUpIndicator(R.drawable.ic_back);
        }

        CollapsingToolbarLayout collapsingToolbar = (CollapsingToolbarLayout) findViewById(R.id.collapsing_toolbar);

        collapsingToolbar.setTitle(getIntent().getStringExtra("name"));

        mLocationClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        mApi = PlacesAPI.getInstance();

        mapFragment = (SupportMapFragment) getSupportFragmentManager()
                .findFragmentById(R.id.mapPoi);

        mMapView = mapFragment.getView();

        mPoints = new ArrayList<>();

        findViewById(R.id.infoContainer).setVisibility(View.GONE);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem menuItem) {
        if (menuItem.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return super.onOptionsItemSelected(menuItem);
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    protected void onStart() {
        mLocationClient.connect();
        super.onStart();
        findViewById(R.id.poiProgressBar).setVisibility(View.VISIBLE);
        mApi.getPlaceById(getIntent().getStringExtra("id"), this);
    }

    @Override
    protected void onStop() {
        mLocationClient.disconnect();
        super.onStop();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onResponse(Object obj) {

        findViewById(R.id.poiProgressBar).setVisibility(View.GONE);
        findViewById(R.id.infoContainer).setVisibility(View.VISIBLE);

        final Place place = (Place) obj;

        final List<PlacePhoto> imageContent = place.getPhotos();

        int pagerPosition = 0;

        if (imageContent != null && imageContent.size() != 0) {
            String[] arr = new String[imageContent.size()];
            for (int i = 0; i < imageContent.size(); i++) {
                arr[i] = imageContent.get(i).getRequestUrl();
            }

            pager.setAdapter(new ImagePagerAdapter(arr));
        } else {
            String imageUri = "drawable://" + R.drawable.ic_missing_img;
            String[] arr = {imageUri};
            pager.setAdapter(new ImagePagerAdapter(arr));
        }

        pager.setOnPageChangeListener(
                new ViewPager.SimpleOnPageChangeListener() {
                    @Override
                    public void onPageSelected(int position) {
                        positionGeral = position + 1;
                    }
                }
        );

        if (mLastLocation != null) {
            mApi.getDistanceToPoi(new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude()),
                    place.getLocation(), new PlacesAPI.PlacesCallback() {
                        @Override
                        public void onResponse(Object obj) {
                            PlaceDirection pD = (PlaceDirection) obj;

                            drawPrimaryLinePath(pD.getDirections(), place.getLocation());

                            ((TextView) findViewById(R.id.poiDistance)).setText(pD.getDistance());
                        }
                    });
        }

        ((RatingBar) findViewById(R.id.poiRating)).setRating(place.getRating());

        mApi.getPlaceDescriptionFromWiki(place.getLocation(), new PlacesAPI.PlacesCallback() {
            @Override
            public void onResponse(Object obj) {
                if (!((String) obj).equals("")) {
                    findViewById(R.id.poiAboutContainer).setVisibility(View.VISIBLE);
                    ((TextView) findViewById(R.id.poiAboutContainer)
                            .findViewById(R.id.textAboutPoi)).setText((String) obj);
                }
            }
        });

        setScheduleViews(place);

        ((TextView) findViewById(R.id.textPoiAddress)).setText(place.getAddress());

        if (place.getWebsite() != null && !place.getWebsite().equals("")) {
            findViewById(R.id.poiWebsiteContainer).setVisibility(View.VISIBLE);
            TextView link = (TextView) findViewById(R.id.textPoiWebsite);
            link.setClickable(true);
            link.setText(place.getWebsite());
            link.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse(place.getWebsite())));
                }
            });
        } else {
            findViewById(R.id.poiWebsiteContainer).setVisibility(View.GONE);
        }

        setupNearbyList(place);
    }

    private void setupNearbyList(final Place place) {
        String locationStr = String.valueOf(place.getLocation().latitude) +
                "," +
                String.valueOf(place.getLocation().longitude);
        PlacesAPI.getInstance().getPlacesNearby(locationStr, "500",
                PlacesAPI.CATEGORY_POI, 0, new PlacesAPI.PlacesCallback() {
                    @Override
                    public void onResponse(Object obj) {

                        final List<Place> nearByPlcs = (List<Place>) obj;

                        final LinearLayout ll = (LinearLayout) findViewById(R.id.poiNearbyList);

                        for (int i = 0; i < nearByPlcs.size(); i++) {

                            final Place iP = nearByPlcs.get(i);

                            if (!iP.getId().equals(place.getId()))
                                if (mLastLocation != null) {
                                    final int finalI = i;
                                    mApi.getDistanceToPoi(new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude()),
                                            iP.getLocation(), new PlacesAPI.PlacesCallback() {
                                                @Override
                                                public void onResponse(Object obj) {

                                                    PlaceDirection pD = (PlaceDirection) obj;
                                                    iP.setDistanceTo(pD.getDistance());
                                                    ll.addView(new NearbyListItemView(ShowMoreInfoActivity.this, iP));
                                                    if (finalI == nearByPlcs.size() - 1) {
                                                        ll.findViewById(R.id.poiListProgress).setVisibility(View.GONE);
                                                    } else {
                                                        View div = new View(ShowMoreInfoActivity.this);
                                                        FrameLayout.LayoutParams divLp =
                                                                new FrameLayout.LayoutParams(FrameLayout.LayoutParams.MATCH_PARENT, DisplayHelper.dpToPx(ShowMoreInfoActivity.this, 1));
                                                        divLp.gravity = Gravity.BOTTOM;
                                                        div.setLayoutParams(divLp);
                                                        div.setBackgroundColor(ShowMoreInfoActivity.this.getResources().getColor(R.color.poiBck));
                                                        ll.addView(div);
                                                    }
                                                }
                                            });
                                }
                        }
                    }
                });
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION)
                != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_COARSE_LOCATION)
                != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mLocationClient);

        mapFragment.getMapAsync(this);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    private void setScheduleViews(Place place) {

        LinearLayout openDaysCont = (LinearLayout) findViewById(R.id.poiOpenDaysContainer);
        LinearLayout openHoursCont = (LinearLayout) findViewById(R.id.poiOpenTimeContainer);

        LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                DisplayHelper.dpToPx(this, 0), DisplayHelper.dpToPx(this, 106));

        if (place.getOpenDays() != -1) {
            openDaysCont.setVisibility(View.VISIBLE);
            params.weight = 1.0f;
            openDaysCont.setLayoutParams(params);
            ((TextView) findViewById(R.id.textPoiOpenDays)).setText(
                    place.getOpenDays() + " " + getResources().getString(R.string.poi_open_days));
        } else {
            openDaysCont.setVisibility(View.GONE);
        }

        if (place.getOpenHours() != null && !place.getOpenHours().equals("")) {
            openHoursCont.setVisibility(View.VISIBLE);
            params.weight = 1.0f;
            openHoursCont.setLayoutParams(params);
            ((TextView) findViewById(R.id.textPoiOpenTime)).setText(place.getOpenHours());
        } else {
            openHoursCont.setVisibility(View.GONE);
        }

        if (openDaysCont.getVisibility() == View.GONE
                && openHoursCont.getVisibility() == View.VISIBLE) {
            params.weight = 2.0f;
            openHoursCont.setLayoutParams(params);
            findViewById(R.id.poiVerticalDivider).setVisibility(View.GONE);
        } else if (openDaysCont.getVisibility() == View.VISIBLE
                && openHoursCont.getVisibility() == View.GONE) {
            params.weight = 2.0f;
            openDaysCont.setLayoutParams(params);
            findViewById(R.id.poiVerticalDivider).setVisibility(View.GONE);
        }

        if (openDaysCont.getVisibility() == View.GONE
                && openHoursCont.getVisibility() == View.GONE) {
            findViewById(R.id.poiScheduleContainer).setVisibility(View.GONE);
        }
    }

    private void drawPrimaryLinePath(List<LatLng> listLocsToDraw, LatLng placeLoc) {

        if (mMap == null) {
            return;
        }

        if (listLocsToDraw.size() < 2) {
            return;
        }

        MarkerOptions m = new MarkerOptions()
                .position(placeLoc)
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_marker));

        mMap.addMarker(m);

        mPoints.add(m.getPosition());
        mPoints.addAll(listLocsToDraw);

        PolylineOptions options = new PolylineOptions();

        options.color(Color.parseColor("#2e5800"));
        options.width(6);
        options.visible(true);

        for (LatLng locRecorded : listLocsToDraw) {
            options.add(locRecorded);
        }
        mMap.addPolyline(options);

        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        for (LatLng point : mPoints) {
            builder.include(point);
        }
        LatLngBounds bounds = builder.build();
        int width = getResources().getDisplayMetrics().widthPixels;
        int height = mapFragment.getView().getLayoutParams().height;
        int padding = (int) (width * 0.10);
        mMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding));
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        googleMap.getUiSettings().setScrollGesturesEnabled(false);

        CameraPosition cameraPosition = new CameraPosition.Builder()
                .target(new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude()))
                .zoom(17)
                .bearing(0)
                .tilt(0)
                .build();

        MarkerOptions marker = new MarkerOptions()
                .position(new LatLng(mLastLocation.getLatitude(), mLastLocation.getLongitude()))
                .icon(BitmapDescriptorFactory.fromResource(R.drawable.ic_walk));

        mPoints.add(marker.getPosition());

        mMap.addMarker(marker);

        mMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
    }

    private class ImagePagerAdapter extends PagerAdapter {

        private String[] images;

        private LayoutInflater inflater;

        ImagePagerAdapter(String[] images) {
            this.images = images;
            inflater = getLayoutInflater();
        }

        @Override
        public void destroyItem(ViewGroup container, int position, Object object) {
            container.removeView((View) object);
        }

        @Override
        public int getCount() {
            return images.length;
        }

        @Override
        public Object instantiateItem(ViewGroup view, int position) {
            View imageLayout = inflater.inflate(R.layout.item_pager_image, view, false);
            assert imageLayout != null;

            ImageView imageView = (ImageView) imageLayout.findViewById(R.id.image);
            imageView.setCropToPadding(true);
            imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);

            final ProgressBar spinner = (ProgressBar) imageLayout.findViewById(R.id.loading);
            spinner.setVisibility(View.VISIBLE);

            ImageLoader.getInstance().displayImage(images[position], imageView, options,
                    new SimpleImageLoadingListener() {
                        @Override
                        public void onLoadingStarted(String imageUri, View view) {
                            spinner.setVisibility(View.VISIBLE);
                        }

                        @Override
                        public void onLoadingFailed(String imageUri, View view,
                                                    FailReason failReason) {
                            String message = null;
                            switch (failReason.getType()) {
                                case IO_ERROR:
                                    message = "Input/Output error";
                                    break;
                                case DECODING_ERROR:
                                    message = "Image can't be decoded";
                                    break;
                                case NETWORK_DENIED:
                                    message = "Downloads are denied";
                                    break;
                                case OUT_OF_MEMORY:
                                    message = "Out Of Memory error";
                                    break;
                                case UNKNOWN:
                                    message = "Unknown error";
                                    break;
                            }
                            //Toast.makeText(ShowMoreInfoActivity.this, message, Toast.LENGTH_SHORT).show();

                            spinner.setVisibility(View.GONE);
                        }

                        @Override
                        public void onLoadingComplete(String imageUri, View view,
                                                      Bitmap loadedImage) {
                            spinner.setVisibility(View.GONE);
                        }
                    });

            view.addView(imageLayout, 0);
            return imageLayout;
        }

        @Override
        public boolean isViewFromObject(View view, Object object) {
            return view.equals(object);
        }

        @Override
        public void restoreState(Parcelable state, ClassLoader loader) {
        }

        @Override
        public Parcelable saveState() {
            return null;
        }
    }
}
