package com.davrbk.tourism.helper;

import android.content.Context;
import android.content.OperationApplicationException;
import android.content.SharedPreferences;
import android.os.RemoteException;

import com.davrbk.tourism.Constants;
import com.davrbk.tourism.api.TourismAPI;
import com.davrbk.tourism.listener.OnResultsListener;
import com.davrbk.tourism.model.domain.CategoryDomain;
import com.davrbk.tourism.view.DialogFilter;
import com.google.android.gms.maps.model.LatLng;

import org.apache.commons.lang3.StringUtils;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;

import citysdk.tourism.client.exceptions.InvalidParameterException;
import citysdk.tourism.client.exceptions.InvalidValueException;
import citysdk.tourism.client.poi.single.POI;
import citysdk.tourism.client.requests.Parameter;
import citysdk.tourism.client.requests.ParameterList;
import citysdk.tourism.client.terms.ParameterTerms;

/**
 * Created by david on 22.12.16.
 */

public class EndpointHelper {

    private Context mCtx;

    private SharedPreferences mPrefs;

    private double lat, lng;

    private final static String SET_CATEGORIES = "categories";

    public EndpointHelper(Context ctx) {
        mCtx = ctx;
        mPrefs = ctx.getSharedPreferences(Constants.PREFS, 0);
    }

    public void changeEndpoint(LatLng newPoint, OnResultsListener listener) {
        LatLng oldPoint = new LatLng(0, 0);

        lat = newPoint.latitude;
        lng = newPoint.longitude;

        List<String> categories = new ArrayList<String>(
                mPrefs.getStringSet(SET_CATEGORIES, new HashSet<String>()));

//        if (categories.size() == 0) {
            new Thread() {
                @Override
                public void run() {
                    getCategories(ParameterTerms.POIS.getTerm());
                }
            }.start();
//        }

        ParameterList list = new ParameterList();
        try {
            list.add(new Parameter(ParameterTerms.LIMIT, 300));
            list.add(new Parameter(ParameterTerms.COORDS, lat + " " + lng + " " + "10000"));
        } catch (InvalidParameterException e) {
            e.printStackTrace();
        } catch (InvalidValueException e) {
            e.printStackTrace();
        }

//        if ((oldPoint.latitude == 0 && oldPoint.longitude == 0)
//                /*|| distanceToPoints(oldPoint, newPoint) > 1000 || rows == 0*/) {
//
//            TourismAPI.getEndpoint((OnResultsListener) this, list);
//
//            //SyncUtils.TriggerRefresh();
//        }

        TourismAPI.getPlaceCategories(mCtx, listener, list, "", "places");
    }
    public void changeCategory(OnResultsListener listener, String category) {

        List<String> selected = new ArrayList<>();

        ParameterList list = new ParameterList();

        List<String> categories = new ArrayList<String>(
                mPrefs.getStringSet(SET_CATEGORIES, new HashSet<String>()));

        switch (category) {
            case DialogFilter.ART:
//                for (String cat : categories) {
//                    if (StringUtils.containsIgnoreCase(cat, "gall")   )
//                }
                break;

            case DialogFilter.HISTORICAL:
                break;

            case DialogFilter.ARCHITECTURE:
                break;

            case DialogFilter.FREE:
                break;

            case DialogFilter.TOP:
                break;

            case DialogFilter.PARKS:
                break;

            case DialogFilter.EAT:
                break;

            case DialogFilter.DRINK:
                break;

            case DialogFilter.HOTEL:
                break;

            case DialogFilter.ROMANTIC:
                break;
        }

        try {
            list.add(new Parameter(ParameterTerms.LIMIT, 300));
            list.add(new Parameter(ParameterTerms.COORDS, lat + " " + lng + " " + "10000"));
            list.add(new Parameter(ParameterTerms.CATEGORY, selected));
        } catch (InvalidParameterException e) {
            e.printStackTrace();
        } catch (InvalidValueException e) {
            e.printStackTrace();
        }

        TourismAPI.getPlaceCategories(mCtx, listener, list, "", "places");
    }

    private void getCategories(String pois) {
        try {
            ParameterList list = new ParameterList();
            list.add(new Parameter(ParameterTerms.LIST, pois));
            list.add(new Parameter(ParameterTerms.LIMIT, -1));

            TourismAPI.getCategories(mCtx, mGetCategoriesListener, list);

        } catch (InvalidParameterException e) {
            e.printStackTrace();
        } catch (InvalidValueException e) {
            e.printStackTrace();
        }
    }

    private OnResultsListener mGetCategoriesListener = new OnResultsListener() {
        @Override
        public void onResultsFinished(POI poi, int id, String parameterTerm, String bytesOfMessage) {
            if (id == 0) {
                if (poi == null) {
                    return;
                }

                List<CategoryDomain> list = TourismAPI.getCategoriesInformation(mCtx, poi,
                        parameterTerm.toString());

                ArrayList<String> categories = new ArrayList<>();

                for (CategoryDomain category : list) {
                    categories.add(category.getName());
                }
                mPrefs.edit().putStringSet(SET_CATEGORIES, new HashSet<String>(categories)).apply();

//                try {
//                   handleCategories(list, parameterTerm);
//                } catch (RemoteException e) {
//                    e.printStackTrace();
//                } catch (OperationApplicationException e) {
//                    e.printStackTrace();
//                }
            }

        }
    };
}
