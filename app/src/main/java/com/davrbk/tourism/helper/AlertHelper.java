package com.davrbk.tourism.helper;

import android.content.Context;
import android.content.DialogInterface;
import android.graphics.Typeface;

import com.afollestad.materialdialogs.DialogAction;
import com.afollestad.materialdialogs.MaterialDialog;
import com.davrbk.tourism.R;

/**
 * Created by david on 18.12.16.
 */

public class AlertHelper {

    public static MaterialDialog getProgressDialog(Context context, String message) {
        MaterialDialog.Builder dialog = new MaterialDialog.Builder(context);
        dialog.cancelable(false);
        dialog.progress(true, 100);
        dialog.widgetColorRes(R.color.colorPrimary);
//        dialog.typeface(Typeface.createFromAsset(context.getAssets(), "fonts/fabric.ttf"),
//                Typeface.createFromAsset(context.getAssets(), "fonts/fabric.ttf"));
        dialog.content(message);

        MaterialDialog md = dialog.build();
        return md;
    }

    public static void showDialogWithTwoButtons(Context context, boolean cancelable, int title, int text, int positive, int negative, final OnActionListener onActionListener) {
        MaterialDialog.Builder dialog = new MaterialDialog.Builder(context);
        if (title != -1)
            dialog.title(context.getResources().getString(title));
        if (text != -1)
            dialog.content(context.getResources().getString(text));
        dialog.positiveText(context.getResources().getString(positive));
        dialog.negativeText(context.getResources().getString(negative));
//        dialog.typeface(Typeface.createFromAsset(context.getAssets(), "fonts/fabric.ttf"),
//                Typeface.createFromAsset(context.getAssets(), "fonts/fabric.ttf"));
        dialog.onPositive(new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(MaterialDialog dialog, DialogAction which) {
                onActionListener.onPositive(null);
            }
        });
        dialog.onNegative(new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(MaterialDialog dialog, DialogAction which) {
                onActionListener.onNegative(null);
            }
        });
        dialog.dismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                onActionListener.onDismiss();
            }
        });
        dialog.cancelable(cancelable);
        dialog.show();
    }

    public static void showDialogWithOneButton(Context context, int title, int text, int positive, final OnActionListener onActionListener) {
        showDialogWithOneButton(context, context.getResources().getString(title), context.getResources().getString(text), positive, onActionListener);
    }

    public static void showDialogWithOneButton(Context context, int title, String text, int positive, final OnActionListener onActionListener) {
        showDialogWithOneButton(context, context.getResources().getString(title), text, positive, onActionListener);
    }

    private static boolean isButtonClicked;

    public static void showDialogWithOneButton(Context context, String title, String text, int positive, final OnActionListener onActionListener) {
        if (context == null) {
            return;
        }
        isButtonClicked = false;
        MaterialDialog.Builder dialog = new MaterialDialog.Builder(context);
        if (title != null)
            dialog.title(title);
        if (text != null)
            dialog.content(text);
//        dialog.typeface(Typeface.createFromAsset(context.getAssets(), "fonts/fabric.ttf"),
//                Typeface.createFromAsset(context.getAssets(), "fonts/fabric.ttf"));
        dialog.positiveText(context.getResources().getString(positive));
        dialog.onPositive(new MaterialDialog.SingleButtonCallback() {
            @Override
            public void onClick(MaterialDialog dialog, DialogAction which) {
                isButtonClicked = true;
                if (onActionListener != null) {
                    onActionListener.onPositive(null);
                }
            }
        });
        dialog.dismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if (onActionListener != null) {
                    if (isButtonClicked) {
                        isButtonClicked = false;
                        return;
                    }
                    onActionListener.onDismiss();
                }
            }
        });
        dialog.cancelable(true);
        dialog.show();
    }

    public static class OnActionListener {
        public void onPositive(Object o) {
        }

        public void onNegative(Object o) {
        }

        public void onDismiss() {
        }
    }
}