package com.davrbk.tourism.helper;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.widget.Toast;

import com.davrbk.tourism.R;

/**
 * Created by david on 20.12.16.
 */

public class NetworkHelper {

    public static final int NONE = 0;
    public static final int TOAST = 1;
    public static final int ALERT = 2;

    public static boolean isOnline(final Context context, final int mode) {
        return isOnline(context, mode, null);
    }

    public static boolean isOnline(final Context context, final int mode, final AlertHelper.OnActionListener listener) {
        if (!hasInternetConnectivity(context)) {
            if (mode == TOAST)
                Toast.makeText(context, context.getResources().getString(
                        R.string.error_internet_connection).replace(".", ""),
                        Toast.LENGTH_LONG).show();
            else if (mode == ALERT)
                AlertHelper.showDialogWithTwoButtons(context, true, R.string.warning,
                        R.string.error_internet_connection,
                        R.string.open_settings, R.string.close, new AlertHelper.OnActionListener() {
                            @Override
                            public void onPositive(Object o) {
                                if (listener != null) {
                                    listener.onPositive(null);
                                }
                                context.startActivity(new Intent(android.provider.Settings.ACTION_WIFI_SETTINGS));
                            }

                            @Override
                            public void onNegative(Object o) {
                                if (listener != null) {
                                    listener.onNegative(null);
                                }
                            }
                        });
            return false;
        }
        return true;
    }

    private static boolean hasInternetConnectivity(Context context) {
        ConnectivityManager connManager = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnected();
    }
}
