package com.davrbk.tourism.helper;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationManager;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;

import com.davrbk.tourism.R;
import com.google.android.gms.maps.model.LatLng;

import java.util.List;

/**
 * Created by david on 16.01.17.
 */

public class LocationHelper {

    public final static String RADIUS_500 = "500";

    public static LatLng getMyLastLocation(Activity activity) {
        LocationManager locationManager = (LocationManager)
                activity.getSystemService(Context.LOCATION_SERVICE);

        Location best = null;

        if (ActivityCompat.checkSelfPermission(activity,
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(activity,
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {

        }

        List<String> providers = locationManager.getProviders(true);
        for (String provider :
                providers) {
            Location l = locationManager.getLastKnownLocation(provider);
            if (l == null)
                continue;

            if (best == null || l.getAccuracy() < best.getAccuracy()) {
                best = l;
            }
        }

        assert best != null;
        return new LatLng(best.getLatitude(), best.getLongitude());
    }

    public static String getMyLastLocationStr(Activity activity) {
        LatLng latLng = getMyLastLocation(activity);
        return latLng.latitude + "," + latLng.longitude;
    }

    public static String currentLocationToStr(Location curr) {
        return curr.getLatitude() + "," + curr.getLongitude();
    }

    public static boolean enabledLocationServices(final Context context, boolean withDialog) {
        LocationManager lm = (LocationManager) context.getSystemService(Context.LOCATION_SERVICE);
        boolean isGps = false;
        boolean isNetwork = false;

        try {
            isGps = lm.isProviderEnabled(LocationManager.GPS_PROVIDER);
        } catch(Exception ex) {}

        try {
            isNetwork = lm.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        } catch(Exception ex) {}

        if ((withDialog) && (!isGps && !isNetwork)) {
            AlertDialog.Builder dialog = new AlertDialog.Builder(context);
            dialog.setMessage(context.getResources().getString(R.string.dialog_gps_not_enabled));
            dialog.setPositiveButton(context.getResources().getString(R.string.open_settings), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {
                    // TODO Auto-generated method stub
                    Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    context.startActivity(myIntent);
                    //get gps
                }
            });
            dialog.setNegativeButton(context.getString(R.string.cancel), new DialogInterface.OnClickListener() {

                @Override
                public void onClick(DialogInterface paramDialogInterface, int paramInt) {

                }
            });
            dialog.show();
        }
        return isGps && isNetwork;
    }
}
