package com.davrbk.tourism.fragment;

import android.Manifest;
import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.location.Location;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.annotation.StringDef;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.afollestad.materialdialogs.MaterialDialog;
import com.davrbk.tourism.R;
import com.davrbk.tourism.Tourism;
import com.davrbk.tourism.activity.MainActivity;
import com.davrbk.tourism.activity.ShowMoreInfoActivity;
import com.davrbk.tourism.api.placesapi.PlacesAPI;
import com.davrbk.tourism.helper.AlertHelper;
import com.davrbk.tourism.helper.DisplayHelper;
import com.davrbk.tourism.helper.LocationHelper;
import com.davrbk.tourism.model.LocationDetails;
import com.davrbk.tourism.model.Marker;
import com.davrbk.tourism.model.Place;
import com.davrbk.tourism.view.DialogFilter;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.LocationSource;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.maps.android.clustering.Cluster;
import com.google.maps.android.clustering.ClusterManager;
import com.google.maps.android.clustering.view.DefaultClusterRenderer;
import com.google.maps.android.ui.IconGenerator;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Comparator;
import java.util.List;

import citysdk.tourism.client.parser.DataReader;
import citysdk.tourism.client.parser.data.GeometryContent;
import citysdk.tourism.client.parser.data.LineContent;
import citysdk.tourism.client.parser.data.LocationContent;
import citysdk.tourism.client.parser.data.PointContent;
import citysdk.tourism.client.parser.data.PolygonContent;
import citysdk.tourism.client.poi.single.POI;
import citysdk.tourism.client.terms.Term;

/**
 * Created by david on 18.12.16.
 */

public class MainMapFragment extends Fragment implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener {

    private GoogleMap mMap;

    private GoogleApiClient locationClient;

    private SupportMapFragment mapFragment;

    private ClusterManager<Marker> mClusterManager;

    private float mLastZoom;

    private boolean mMyLocationButtonClicked = false;

    private View mMapView;

    private List<Place> mLastPlaces;

    private DialogFilter mFilterDialog;

    private Location mCurrentLocation;

    private GoogleMap.OnMyLocationChangeListener mLocationChangedListener = new GoogleMap.OnMyLocationChangeListener() {
        @Override
        public void onMyLocationChange(Location location) {
            mCurrentLocation = location;

            if (mMyLocationButtonClicked) {

                String locationStr = String.valueOf(location.getLatitude()) +
                        "," +
                        String.valueOf(location.getLongitude());;

                PlacesAPI.getInstance().getPlacesNearby(locationStr, "500",
                        PlacesAPI.CATEGORY_POI, 0, new PlacesAPI.PlacesCallback() {
                            @Override
                            public void onResponse(Object obj) {
                                showMrkr((List<Place>) obj);
                                mProgress.dismiss();
                                DisplayHelper.animateToMeters(getActivity(), new LatLng(mCurrentLocation.getLatitude(),
                                        mCurrentLocation.getLongitude()), 500, mMap);
                            }
                        });
                mMyLocationButtonClicked = false;
            }
        }
    };

    private ClusterManager.OnClusterClickListener<Marker> mOnClusterClickListener = new ClusterManager.OnClusterClickListener<Marker>() {
        @Override
        public boolean onClusterClick(Cluster<Marker> cluster) {
            CharSequence[] arr = new CharSequence[cluster.getSize()];

            Collection<Marker> items = cluster.getItems();
            java.util.Collections.sort((List<Marker>) items, new Comparator<Marker>() {
                @Override
                public int compare(Marker lhs, Marker rhs) {
                    return lhs.getName().compareTo(rhs.getName());
                }
            });


            final Marker[] list = items.toArray(new Marker[items.size()]);
            for (int i = 0; i < list.length; i++) {
                arr[i] = list[i].getName();
            }

            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("More Information")
                    .setItems(arr, new DialogInterface.OnClickListener() {
                        public void onClick(DialogInterface dialog, int which) {
                            callMoreInfo(list[which]);

                        }
                    });
            builder.show();

            return true;
        }
    };

    private ClusterManager.OnClusterItemClickListener<Marker> mOnClusterItemClickListener = new ClusterManager.OnClusterItemClickListener<Marker>() {
        @Override
        public boolean onClusterItemClick(Marker marker) {
            callMoreInfo(marker);
            return true;
        }
    };

    GoogleMap.OnCameraIdleListener mOnCameraIdleListener = new GoogleMap.OnCameraIdleListener() {
        @Override
        public void onCameraIdle() {
            CameraPosition position = mMap.getCameraPosition();
            if (position.zoom != mLastZoom) {
                showMrkr(mLastPlaces);
                mLastZoom = position.zoom;
            }
        }
    };

    MaterialDialog mProgress;

    PlacesAPI.PlacesCallback mFilterCallback = new PlacesAPI.PlacesCallback() {
        @Override
        public void onResponse(Object obj) {
            if (((List<Place>) obj).size() == 0) {
                Toast.makeText(getActivity(), R.string.toast_zero, Toast.LENGTH_SHORT).show();
            }
            showMrkr((List<Place>) obj);
            if (mProgress.isShowing())
                mProgress.dismiss();

            DisplayHelper.animateToMeters(getActivity(),
                    new LatLng(mCurrentLocation.getLatitude(), mCurrentLocation.getLongitude()), 500, mMap);
        }
    };

    private DialogFilter.OnDialogItemCheckedListener mOnDialogItemCheckedListener = new DialogFilter.OnDialogItemCheckedListener() {
        @Override
        public void onArtChecked() {
            //art_gallery,movie_theater,painter,zoo
            PlacesAPI.getInstance()
                    .getPlacesNearby(LocationHelper.currentLocationToStr(mCurrentLocation),
                            LocationHelper.RADIUS_500, PlacesAPI.CATEGORY_ART, 0, mFilterCallback);
            if (!mProgress.isShowing())
                mProgress.show();
        }

        @Override
        public void onHistoricalChecked() {
//            mEndpointHelper.changeCategory(mOnResultsListener, DialogFilter.HISTORICAL);
            //museum
            PlacesAPI.getInstance()
                    .getPlacesNearby(LocationHelper.currentLocationToStr(mCurrentLocation),
                            LocationHelper.RADIUS_500, PlacesAPI.CATEGORY_MUSEUM, 0, mFilterCallback);
            if (!mProgress.isShowing())
                mProgress.show();
        }

        @Override
        public void onArchitectureChecked() {
            //point_of_interest
            PlacesAPI.getInstance()
                    .getPlacesNearby(LocationHelper.currentLocationToStr(mCurrentLocation),
                            LocationHelper.RADIUS_500, PlacesAPI.CATEGORY_POI, 0, mFilterCallback);
            if (!mProgress.isShowing())
                mProgress.show();
        }

        @Override
        public void onFreeChecked() {
            PlacesAPI.getInstance()
                    .getPlacesNearby(LocationHelper.currentLocationToStr(mCurrentLocation),
                            LocationHelper.RADIUS_500, PlacesAPI.CATEGORY_POI, 0, mFilterCallback);
            if (!mProgress.isShowing())
                mProgress.show();
        }

        @Override
        public void onTopChecked() {
            //get places with rating range 4-5
            PlacesAPI.getInstance()
                    .getPlacesNearby(LocationHelper.currentLocationToStr(mCurrentLocation),
                            LocationHelper.RADIUS_500, PlacesAPI.CATEGORY_POI, PlacesAPI.FLAG_TOP_RATED,
                            mFilterCallback);
            if (!mProgress.isShowing())
                mProgress.show();
        }

        @Override
        public void onParksChecked() {
            //park,zoo,campground
            PlacesAPI.getInstance()
                    .getPlacesNearby(LocationHelper.currentLocationToStr(mCurrentLocation),
                            LocationHelper.RADIUS_500, PlacesAPI.CATEGORY_PARK, 0, mFilterCallback);
            if (!mProgress.isShowing())
                mProgress.show();
        }

        @Override
        public void onEatChecked() {
            //food,cafe,restaurant,bakery
            PlacesAPI.getInstance()
                    .getPlacesNearby(LocationHelper.currentLocationToStr(mCurrentLocation),
                            LocationHelper.RADIUS_500, PlacesAPI.CATEGORY_FOOD, 0, mFilterCallback);
            if (!mProgress.isShowing())
                mProgress.show();

        }

        @Override
        public void onDrinkChecked() {
            //bar,cafe
            PlacesAPI.getInstance()
                    .getPlacesNearby(LocationHelper.currentLocationToStr(mCurrentLocation),
                            LocationHelper.RADIUS_500, PlacesAPI.CATEGORY_DRINK, 0, mFilterCallback);
            if (!mProgress.isShowing())
                mProgress.show();
        }

        @Override
        public void onHotelChecked() {
            //lodging
            PlacesAPI.getInstance()
                    .getPlacesNearby(LocationHelper.currentLocationToStr(mCurrentLocation),
                            LocationHelper.RADIUS_500, PlacesAPI.CATEGORY_HOTEL, 0, mFilterCallback);
            if (!mProgress.isShowing())
                mProgress.show();
        }

        @Override
        public void onRomanticChecked() {
            PlacesAPI.getInstance()
                    .getPlacesNearby(LocationHelper.currentLocationToStr(mCurrentLocation),
                            LocationHelper.RADIUS_500, "park", 0, mFilterCallback);
            if (!mProgress.isShowing())
                mProgress.show();
        }
    };

    private boolean myPositionFlag = false;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = ((LayoutInflater) getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE))
                .inflate(R.layout.fragment_main_map, null);

        mapFragment = (SupportMapFragment) getActivity().getSupportFragmentManager()
                .findFragmentById(R.id.map);

        mMapView = mapFragment.getView();

        mProgress = AlertHelper.getProgressDialog(getActivity(),
                getActivity().getResources().getString(R.string.progress_wait));

        locationClient = new GoogleApiClient.Builder(getActivity())
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        mFilterDialog = new DialogFilter(getActivity(), R.style.DialogSlide);
        mFilterDialog.setCancelable(true);
        mFilterDialog.setOnDialogItemCheckedListener(mOnDialogItemCheckedListener);

        return v;
    }

    private void setupMap() {

        //		if(map != null && mClusterManager != null && locationClient != null) {
        //			return;
        //		}
        SharedPreferences prefs = Tourism.getInstance().getAppsSharedPreferences();

        final float zoom = prefs.getFloat("zoom", 17);
        final float tilt = prefs.getFloat("tilt", 30);
        final float latitude = prefs.getFloat("latitude", 0);
        final float longitude = prefs.getFloat("longitude", 0);
        final float bearing = prefs.getFloat("bearing", 90);

        mapFragment.getMapAsync(new OnMapReadyCallback() {
            @Override
            public void onMapReady(GoogleMap googleMap) {
                mMap = googleMap;

                mLastZoom = googleMap.getCameraPosition().zoom;

                if (mClusterManager == null) {
                    mClusterManager = new ClusterManager<Marker>(getActivity(), mMap);
                }
                if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                        && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    return;
                }
                mMap.setMyLocationEnabled(true);

                mMap.setOnMyLocationButtonClickListener(new GoogleMap.OnMyLocationButtonClickListener() {
                    @Override
                    public boolean onMyLocationButtonClick() {
                        if (LocationHelper.enabledLocationServices(getActivity(), true)) {
                            mMyLocationButtonClicked = true;
                            myPositionFlag = false;
                            mProgress.show();
                        }
                        return true;
                    }
                });

                if (LocationHelper.enabledLocationServices(getActivity(), true)) {

                    mMap.setOnMyLocationChangeListener(mLocationChangedListener);

//                    mMap.setOnMyLocationChangeListener(new GoogleMap.OnMyLocationChangeListener() {
//                        @Override
//                        public void onMyLocationChange(final Location location) {
//
//                            mCurrentLocation = location;
//
//                            if (mMyLocationButtonClicked) {
//
//                                String locationStr = String.valueOf(location.getLatitude()) +
//                                        "," +
//                                        String.valueOf(location.getLongitude());
//
//                                PlacesAPI.getInstance().getPlacesNearby(locationStr, "500",
//                                        PlacesAPI.CATEGORY_POI, 0, new PlacesAPI.PlacesCallback() {
//                                            @Override
//                                            public void onResponse(Object obj) {
//                                                showMrkr((List<Place>) obj);
//                                                mProgress.dismiss();
//                                                DisplayHelper.animateToMeters(getActivity(), new LatLng(mCurrentLocation.getLatitude(),
//                                                        mCurrentLocation.getLongitude()), 500, mMap);
//                                            }
//                                        });
//                                mMyLocationButtonClicked = false;
//
////                                CameraPosition cameraPosition = new CameraPosition.Builder()
////                                        .target(new LatLng(location.getLatitude(),
////                                                location.getLongitude()))
////                                        .zoom(zoom)
////                                        .bearing(0)
////                                        .tilt(0)
////                                        .build();
////                                mMap.moveCamera(CameraUpdateFactory.newCameraPosition(cameraPosition));
//                            }
//                        }
//                    });
                }

                mMap.setIndoorEnabled(true);

                mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);

                mMap.setOnMarkerClickListener(mClusterManager);
                mMap.setOnCameraIdleListener(mOnCameraIdleListener);
                mClusterManager.setOnClusterClickListener(mOnClusterClickListener);
                mClusterManager.setOnClusterItemClickListener(mOnClusterItemClickListener);
                mClusterManager.setRenderer(new MarkerRenderer());

                if (mMapView != null &&
                        mMapView.findViewById(Integer.parseInt("1")) != null)
                    initLocationButton();


                locationClient.connect();
            }
        });
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onStart() {
        super.onStart();
        setupMap();
        MainActivity.getInstance().getTitleBarView().toggleMoreVisibility(true);
        MainActivity.getInstance().getTitleBarView().setOnMoreClicked(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mFilterDialog.show();
            }
        });
    }

    @Override
    public void onStop() {
        super.onStop();
        MainActivity.getInstance().getTitleBarView().toggleMoreVisibility(false);
        MainActivity.getInstance().getTitleBarView().setOnMoreClicked(null);
        SharedPreferences.Editor editor = Tourism.getInstance().getAppsSharedPreferences().edit();
        //editor.clear();
        if (mMap != null) {
            editor.putFloat("zoom", mMap.getCameraPosition().zoom);
            editor.putFloat("tilt", mMap.getCameraPosition().tilt);
            editor.putFloat("latitude", (float) mMap.getCameraPosition().target.latitude);
            editor.putFloat("longitude", (float) mMap.getCameraPosition().target.longitude);
            editor.putFloat("bearing", mMap.getCameraPosition().bearing);
        }
        editor.commit();

        locationClient.disconnect();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {

        if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }
        Location loc = LocationServices.FusedLocationApi.getLastLocation(locationClient);
        if (loc == null) {
            return;
        }
        LatLng coord = new LatLng(loc.getLatitude(), loc.getLongitude());

        if (!myPositionFlag) {
            myPositionFlag = true;
//            CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLngZoom(coord, 17);
//            mMap.animateCamera(cameraUpdate);
            DisplayHelper.animateToMeters(getActivity(), coord, 500, mMap);
        }

    }

    public void clearMarker() {
        if (mapFragment != null) {
//            map = fragment.getMap();
            mapFragment.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap) {
                    mMap = googleMap;

                    if (mMap != null) {
                        mMap.clear();
//                drawSearchPoint(null);

                    }
                    if (mClusterManager != null) {
                        mClusterManager.clearItems();
                    }
                }
            });
        }
    }

    private void showMrkr(List<Place> places) {

        mLastPlaces = places;

        if (mapFragment == null/* || fragment.getMap() == null*/) {
            return;
        }

        if (places == null || places.size() == 0) {
            clearMarker();
            return;
        }

        if (mClusterManager == null) {
            setupMap();
        }

        clearMarker();

        for (int i = 0; i < places.size(); i++) {
            Place p = places.get(i);
            mClusterManager.addItem(new Marker(p.getLocation().latitude,
                    p.getLocation().longitude, p.getName(), p.getTypes()[0], p.getId(), ""));
        }
        mClusterManager.cluster();
    }

    private LatLng centroPosition(List<LatLng> points) {
        double[] centroid = {0.0, 0.0};

        for (int i = 0; i < points.size(); i++) {
            centroid[0] += points.get(i).latitude;
            centroid[1] += points.get(i).longitude;
        }

        int totalPoints = points.size();
        centroid[0] = centroid[0] / totalPoints;
        centroid[1] = centroid[1] / totalPoints;

        return new LatLng(centroid[0], centroid[1]);
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }

    private void callMoreInfo(Marker item) {
        Intent i = new Intent(getActivity(), ShowMoreInfoActivity.class);
        i.putExtra("id", item.getId());
        i.putExtra("base", item.getBase());
        i.putExtra("name", item.getName());
        i.putExtra("category", item.getCategory());
        startActivity(i);
    }

    private void initLocationButton() {
        ImageView locationButton = (ImageView) ((View) mMapView.findViewById(Integer.parseInt("1")).getParent()).findViewById(Integer.parseInt("2"));

        RelativeLayout.LayoutParams layoutParams = (RelativeLayout.LayoutParams)
                locationButton.getLayoutParams();

        locationButton.setImageResource(-1);

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            locationButton.setBackground(getActivity().getDrawable(R.drawable.my_location_bck));
        } else {

            locationButton.setBackground(getActivity().getResources().getDrawable(R.drawable.my_location_bck));
        }

        layoutParams.addRule(RelativeLayout.ALIGN_PARENT_TOP, 0);
        layoutParams.addRule(RelativeLayout.ALIGN_PARENT_BOTTOM, RelativeLayout.TRUE);
        layoutParams.addRule(RelativeLayout.ALIGN_END, RelativeLayout.TRUE);
        layoutParams.setMargins(0, 0, 30, 30);
    }

    private class MarkerRenderer extends DefaultClusterRenderer<Marker> {

        private final IconGenerator mIconGenerator = new IconGenerator(getActivity());

        private String mTextName = "";

        public MarkerRenderer() {
            super(getActivity(), mMap, mClusterManager);

        }

        @Override
        protected void onBeforeClusterItemRendered(Marker person, MarkerOptions markerOptions) {

            mTextName = person.getName();

            View multiProfile = getActivity().getLayoutInflater()
                    .inflate(R.layout.act_maps_marker, null);
            TextView name = (TextView) multiProfile.findViewById(R.id.act_maps_marker_name);
            name.setText(mTextName);

            mIconGenerator.setContentView(multiProfile);

            Bitmap icon = mIconGenerator.makeIcon();
            markerOptions.icon(BitmapDescriptorFactory.fromBitmap(icon)).title(mTextName);
        }

        @Override
        protected boolean shouldRenderAsCluster(Cluster cluster) {
            return cluster.getSize() > 1;
        }
    }
}
